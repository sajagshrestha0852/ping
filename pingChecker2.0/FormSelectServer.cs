﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pingChecker2._0
{
	public partial class FormSelectServer : Form
	{
		public FormSelectServer()
		{
			InitializeComponent();
		}

		private void ButtonOk_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void ButtonOk_MouseHover(object sender, EventArgs e)
		{
			buttonOk.BackColor = Color.FromArgb(255, 64, 68, 75);
		}
		private void ButtonOk_MouseLeave(object sender, EventArgs e)
		{
			buttonOk.BackColor = Color.Transparent;
		}

		private void ButtonOk_MouseEnter(object sender, EventArgs e)
		{
			buttonOk.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

	}
}
