﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pingChecker2._0
{
	public partial class FormNoInternet : Form
	{
		public FormNoInternet()
		{
			InitializeComponent();
		}

		private void ButtonOk_MouseHover(object sender, EventArgs e)
		{
			buttonOkNo.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonOk_MouseLeave(object sender, EventArgs e)
		{
			buttonOkNo.BackColor = Color.Transparent;
		}

		private void ButtonOk_MouseEnter(object sender, EventArgs e)
		{
			buttonOkNo.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonOkNo_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
	
}
