﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.NetworkInformation;


namespace pingChecker2._0
{
	public partial class mainWindow : Form
	{
        private Ping myPing = new Ping();
        private PingReply Reply;
        private byte[] buffer = Encoding.ASCII.GetBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        private PingOptions options = new PingOptions();
        private pingInfo pingVariable = new pingInfo();
        
        bool drag = false;
		Point start_point = new Point(0, 0);
		public mainWindow()
		{
			InitializeComponent();
		}

		private void ButtonClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}


		private void mDown(object sender, MouseEventArgs e)
		{
			drag = true; //drag is your variable flag.
			start_point = new Point(e.X, e.Y);
		}

		private void mMove(object sender, MouseEventArgs e)
		{
			if (drag)
			{
				Point p = PointToScreen(e.Location);
				this.Location = new Point(p.X - start_point.X, p.Y - start_point.Y);
			}
		}

		private void mUp(object sender, MouseEventArgs e)
		{
			drag = false;
		}

		private void closeHover(object sender, EventArgs e)
		{
			buttonClose.BackColor = Color.OrangeRed;
		}

		private void closeLeave(object sender, EventArgs e)
		{
			buttonClose.BackColor = Color.Transparent;

		}

		private void closeEnter(object sender, EventArgs e)
		{
			buttonClose.BackColor = Color.OrangeRed;

		}

		private void dotaEnter(object sender, EventArgs e)
		{
			buttonDota.BackColor = Color.FromArgb(255,64,68,75);
		}

		private void dotaHover(object sender, EventArgs e)
		{
			buttonDota.BackColor = Color.FromArgb(255,64,68,75);
		}
		private void ButtonDota_MouseLeave(object sender, EventArgs e)
		{
			buttonDota.BackColor = Color.Transparent;
		}
	

		private void ButtonDota_Click(object sender, EventArgs e)
		{
			panelDota.Visible = true;
			panelPubg.Visible = false;
			panel1.Height = buttonDota.Height;
			panel1.Top = buttonDota.Top;
            clearFields();
		}

		private void ButtonPubg_MouseEnter(object sender, EventArgs e)
		{
			buttonPubg.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonPubg_MouseHover(object sender, EventArgs e)
		{
			buttonPubg.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonPubg_MouseLeave(object sender, EventArgs e)
		{
			buttonPubg.BackColor = Color.Transparent;
		}

		private void ButtonPubg_Click(object sender, EventArgs e)
		{
			panelPubg.Visible = true;
			panel1.Height = buttonPubg.Height;
			panel1.Top = buttonPubg.Top;
            clearFields();
		}

		private void ButtonApex_MouseEnter(object sender, EventArgs e)
		{
			buttonApex.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonApex_MouseHover(object sender, EventArgs e)
		{
			buttonApex.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonApex_MouseLeave(object sender, EventArgs e)
		{
			buttonApex.BackColor = Color.Transparent;
		}

		private void ButtonApex_Click(object sender, EventArgs e)
		{
			panel1.Height = buttonApex.Height;
			panel1.Top = buttonApex.Top;
		}

		private void ButtonMin_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}

		private void minHover(object sender, EventArgs e)
		{
			buttonMinimize.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void minLeave(object sender, EventArgs e)
		{
			buttonMinimize.BackColor = Color.Transparent;
		}

		private void minEnter(object sender, EventArgs e)
		{
			buttonMinimize.BackColor = Color.FromArgb(255, 64, 68, 75);
		}
        private void buttonCheckPingPubg_Click(object sender, EventArgs e)
        {
            if (comboBoxPubg.SelectedIndex > -1)
            {
                pingVariable.setServerIP(comboBoxPubg.SelectedIndex + 20);
                checkPing();
            }
            else
            {
                ServerNotSelected();
            }

        }
        private void ButtonCheckPingDota_Click(object sender, EventArgs e)
        {
            if (comboBoxDota2.SelectedIndex > -1)
            {
                pingVariable.setServerIP(comboBoxDota2.SelectedIndex);
                checkPing();

            }
            else
            {
                ServerNotSelected();
            }
			
        }
        private void clearFields()
        {
            timer1.Enabled = false;
            pingVariable.ResetAll();
            labelPing.Text = "";
            labelPacketLoss.Text = "";
            labelAvgPing.Text = "";
            labelMaxPing.Text = "";
            labelMinPing.Text = "";
        }
        private void checkPing()
        {
            clearFields();
            timer1.Enabled = true;
        }
        private void printPingInfo()
        {
            pingVariable.calculatePacketLoss();
            pingVariable.calculateAveragePing();
			changeColor(labelAvgPing,pingVariable.averagePing);
            labelAvgPing.Text = pingVariable.averagePing.ToString() + "ms";
			if (pingVariable.packetLoss == 0)
			{
				labelPacketLoss.ForeColor = Color.FromArgb(67, 181, 129);
			}

			else
			{
				labelPacketLoss.ForeColor = System.Drawing.Color.OrangeRed;
			}
			labelPacketLoss.Text = pingVariable.packetLoss.ToString() + "%";
			changeColor(labelMaxPing, pingVariable.MaxPing);
			labelMaxPing.Text = pingVariable.MaxPing.ToString() + "ms";
			changeColor(labelMinPing, pingVariable.MinPing);
			labelMinPing.Text = pingVariable.MinPing.ToString() + "ms";
		}
		public void changeColor(Label labelP,int  ping)
		{
			if (ping < 100)
			{
				labelP.ForeColor = Color.FromArgb(67,181, 129);
			}
			
			else
			{
				labelP.ForeColor = System.Drawing.Color.OrangeRed;
			}
		}
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (pingVariable.PingAmount <=0)
                {
                    timer1.Enabled = false;
                    printPingInfo();
                }
                Reply = myPing.Send(pingVariable.serverIp, 1000, buffer, 
                options);

				if (Reply != null)
                {
                    if (Reply.Status.ToString() == "TimedOut")
                    {
                        pingVariable.failedPings++;
                        labelPing.Text = "TimeOut";
                    }
                   else
                    {

                        if (Reply.Status != IPStatus.Success)
                            pingVariable.failedPings++;
                        else
                        {
							if((int)Reply.RoundtripTime <100 )
							{
								labelPing.ForeColor = Color.FromArgb(67, 181, 129);
							}
							else
							{
								labelPing.ForeColor = System.Drawing.Color.OrangeRed;
							}
							labelPing.Text = Reply.RoundtripTime.ToString() + "ms";
                            pingVariable.latencySum += (int)Reply.RoundtripTime;
                            if (pingVariable.PingAmount == 10)
                            {
                                pingVariable.MaxPing = (int)Reply.RoundtripTime;
                                pingVariable.MinPing = (int)Reply.RoundtripTime;

                            }
                            else
                            {
                                if ((int)Reply.RoundtripTime > pingVariable.MaxPing)
                                    pingVariable.MaxPing = (int)Reply.RoundtripTime;

                                if ((int)Reply.RoundtripTime < pingVariable.MinPing)
                                    pingVariable.MinPing = (int)Reply.RoundtripTime;
                            }
                        }
                  }
                    
                }
			

				pingVariable.PingAmount--;
            }
            catch
            {
				timer1.Enabled = false;
				noInterNet();

			}
		}

        // when there is no internet connection
        private void noInterNet()
        {
			FormNoInternet F1 = new FormNoInternet();
			F1.ShowDialog();
		}
        
       
		private void ButtonCheckPingPubg_MouseLeave(object sender, EventArgs e)
		{
			buttonCheckPingPubg.BackColor = Color.Transparent;

		}

		private void ButtonCheckPingPubg_MouseHover(object sender, EventArgs e)
		{
			buttonCheckPingPubg.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonCheckPingPubg_MouseEnter(object sender, EventArgs e)
		{
			buttonCheckPingPubg.BackColor = Color.FromArgb(255, 64, 68, 75);
		}


		private void ButtonCheckPingDota_MouseHover(object sender, EventArgs e)
		{
			buttonCheckPingDota.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

		private void ButtonCheckPingDota_MouseLeave(object sender, EventArgs e)
		{
			buttonCheckPingDota.BackColor = Color.Transparent;
		}

		private void ButtonCheckPingDota_MouseEnter(object sender, EventArgs e)
		{
			buttonCheckPingDota.BackColor = Color.FromArgb(255, 64, 68, 75);
		}

        //when server is not selected
        private void ServerNotSelected()
        {
			timer1.Enabled = false;

			FormSelectServer F2 = new FormSelectServer();
			F2.ShowDialog();
			
		}


	}
}
