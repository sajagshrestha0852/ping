﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pingChecker2._0
{
    class pingInfo
    {
        
        public int packetLoss;
        public int failedPings;
        public int latencySum;
        public int averagePing;

        public int MaxPing { get; set; }
        public int MinPing { get; set; }
        public int PingAmount { get; set; }
           
        private readonly string[] dota2ServerIp = {  "dxb.valve.net", "sgp-1.valve.net", "sto.valve.net",
            "vie.valve.net", "lux.valve.net", "3.104.0.0", "197.80.200.1", "209.197.25.1", "iad.valve.net", "eat.valve.net" };

        private readonly string[] pubgServerIp = { "18.140.0.0", "54.239.55.155", "177.72.245.184", "52.95.18.3", "54.240.195.243","52.95.193.80", "54.239.96.170" };
        public string serverIp;
        public void setServerIP(int x)
        {
            if(x<20)
            { serverIp = dota2ServerIp[x]; }
            else { serverIp = pubgServerIp[x-20]; }
        }

        public void calculatePacketLoss()
        {
            packetLoss = Convert.ToInt32((Convert.ToDouble(failedPings) / Convert.ToDouble(10)) * 100);

        }
        public void calculateAveragePing()
        {
            averagePing = latencySum / (10-failedPings);
        }
        public void ResetAll()
        {
            packetLoss = 0;
            failedPings = 0;
            latencySum = 0;
            PingAmount = 10;
        }
    }
}
