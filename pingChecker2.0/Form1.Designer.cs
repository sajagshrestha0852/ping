﻿namespace pingChecker2._0
{
	partial class mainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainWindow));
			this.dashBoard = new System.Windows.Forms.Panel();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonApex = new System.Windows.Forms.Button();
			this.buttonPubg = new System.Windows.Forms.Button();
			this.buttonDota = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonClose = new System.Windows.Forms.Button();
			this.topPanel = new System.Windows.Forms.Panel();
			this.labelLogo = new System.Windows.Forms.Label();
			this.buttonMinimize = new System.Windows.Forms.Button();
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
			this.panel3 = new System.Windows.Forms.Panel();
			this.labelPing = new System.Windows.Forms.Label();
			this.labelPacketLoss = new System.Windows.Forms.Label();
			this.labelAvgPing = new System.Windows.Forms.Label();
			this.labelMinPing = new System.Windows.Forms.Label();
			this.labelMaxPing = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.panelDota = new System.Windows.Forms.Panel();
			this.panelPubg = new System.Windows.Forms.Panel();
			this.buttonCheckPingPubg = new System.Windows.Forms.Button();
			this.label14 = new System.Windows.Forms.Label();
			this.comboBoxPubg = new System.Windows.Forms.ComboBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.buttonCheckPingDota = new System.Windows.Forms.Button();
			this.label15 = new System.Windows.Forms.Label();
			this.comboBoxDota2 = new System.Windows.Forms.ComboBox();
			this.pictureDota = new System.Windows.Forms.PictureBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.dashBoard.SuspendLayout();
			this.topPanel.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panelDota.SuspendLayout();
			this.panelPubg.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureDota)).BeginInit();
			this.SuspendLayout();
			// 
			// dashBoard
			// 
			this.dashBoard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(45)))), ((int)(((byte)(50)))));
			this.dashBoard.Controls.Add(this.button4);
			this.dashBoard.Controls.Add(this.button3);
			this.dashBoard.Controls.Add(this.button2);
			this.dashBoard.Controls.Add(this.button1);
			this.dashBoard.Controls.Add(this.panel1);
			this.dashBoard.Controls.Add(this.buttonApex);
			this.dashBoard.Controls.Add(this.buttonPubg);
			this.dashBoard.Controls.Add(this.buttonDota);
			this.dashBoard.Controls.Add(this.label2);
			this.dashBoard.Dock = System.Windows.Forms.DockStyle.Left;
			this.dashBoard.Location = new System.Drawing.Point(0, 0);
			this.dashBoard.Name = "dashBoard";
			this.dashBoard.Size = new System.Drawing.Size(189, 532);
			this.dashBoard.TabIndex = 0;
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.Transparent;
			this.button4.Enabled = false;
			this.button4.FlatAppearance.BorderSize = 0;
			this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button4.ForeColor = System.Drawing.Color.White;
			this.button4.Location = new System.Drawing.Point(14, 188);
			this.button4.Margin = new System.Windows.Forms.Padding(0);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(175, 36);
			this.button4.TabIndex = 8;
			this.button4.Text = "CSGO";
			this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button4.UseVisualStyleBackColor = false;
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.Transparent;
			this.button3.Enabled = false;
			this.button3.FlatAppearance.BorderSize = 0;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.ForeColor = System.Drawing.Color.White;
			this.button3.Location = new System.Drawing.Point(17, 303);
			this.button3.Margin = new System.Windows.Forms.Padding(0);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(175, 36);
			this.button3.TabIndex = 7;
			this.button3.Text = "WARFRAME";
			this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button3.UseVisualStyleBackColor = false;
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Transparent;
			this.button2.Enabled = false;
			this.button2.FlatAppearance.BorderSize = 0;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.ForeColor = System.Drawing.Color.White;
			this.button2.Location = new System.Drawing.Point(17, 267);
			this.button2.Margin = new System.Windows.Forms.Padding(0);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(175, 36);
			this.button2.TabIndex = 6;
			this.button2.Text = "RAINBOW SIX SIEGE";
			this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button2.UseVisualStyleBackColor = false;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Transparent;
			this.button1.Enabled = false;
			this.button1.FlatAppearance.BorderSize = 0;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Location = new System.Drawing.Point(17, 224);
			this.button1.Margin = new System.Windows.Forms.Padding(0);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(175, 36);
			this.button1.TabIndex = 5;
			this.button1.Text = "OVERWATCH";
			this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button1.UseVisualStyleBackColor = false;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Location = new System.Drawing.Point(183, 80);
			this.panel1.Margin = new System.Windows.Forms.Padding(0);
			this.panel1.Name = "panel1";
			this.panel1.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.panel1.Size = new System.Drawing.Size(6, 36);
			this.panel1.TabIndex = 4;
			// 
			// buttonApex
			// 
			this.buttonApex.BackColor = System.Drawing.Color.Transparent;
			this.buttonApex.Enabled = false;
			this.buttonApex.FlatAppearance.BorderSize = 0;
			this.buttonApex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonApex.ForeColor = System.Drawing.Color.White;
			this.buttonApex.Location = new System.Drawing.Point(14, 152);
			this.buttonApex.Margin = new System.Windows.Forms.Padding(0);
			this.buttonApex.Name = "buttonApex";
			this.buttonApex.Size = new System.Drawing.Size(175, 36);
			this.buttonApex.TabIndex = 4;
			this.buttonApex.Text = "APEX LEGENDS";
			this.buttonApex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonApex.UseVisualStyleBackColor = false;
			this.buttonApex.Click += new System.EventHandler(this.ButtonApex_Click);
			this.buttonApex.MouseEnter += new System.EventHandler(this.ButtonApex_MouseEnter);
			this.buttonApex.MouseLeave += new System.EventHandler(this.ButtonApex_MouseLeave);
			this.buttonApex.MouseHover += new System.EventHandler(this.ButtonApex_MouseHover);
			// 
			// buttonPubg
			// 
			this.buttonPubg.BackColor = System.Drawing.Color.Transparent;
			this.buttonPubg.FlatAppearance.BorderSize = 0;
			this.buttonPubg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonPubg.ForeColor = System.Drawing.Color.White;
			this.buttonPubg.Location = new System.Drawing.Point(0, 116);
			this.buttonPubg.Margin = new System.Windows.Forms.Padding(0);
			this.buttonPubg.Name = "buttonPubg";
			this.buttonPubg.Size = new System.Drawing.Size(189, 36);
			this.buttonPubg.TabIndex = 4;
			this.buttonPubg.Text = "    PUBG";
			this.buttonPubg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonPubg.UseVisualStyleBackColor = false;
			this.buttonPubg.Click += new System.EventHandler(this.ButtonPubg_Click);
			this.buttonPubg.MouseEnter += new System.EventHandler(this.ButtonPubg_MouseEnter);
			this.buttonPubg.MouseLeave += new System.EventHandler(this.ButtonPubg_MouseLeave);
			this.buttonPubg.MouseHover += new System.EventHandler(this.ButtonPubg_MouseHover);
			// 
			// buttonDota
			// 
			this.buttonDota.BackColor = System.Drawing.Color.Transparent;
			this.buttonDota.FlatAppearance.BorderSize = 0;
			this.buttonDota.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonDota.ForeColor = System.Drawing.Color.White;
			this.buttonDota.Location = new System.Drawing.Point(0, 80);
			this.buttonDota.Margin = new System.Windows.Forms.Padding(0);
			this.buttonDota.Name = "buttonDota";
			this.buttonDota.Size = new System.Drawing.Size(189, 36);
			this.buttonDota.TabIndex = 4;
			this.buttonDota.Text = "    DOTA2";
			this.buttonDota.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonDota.UseVisualStyleBackColor = false;
			this.buttonDota.Click += new System.EventHandler(this.ButtonDota_Click);
			this.buttonDota.MouseEnter += new System.EventHandler(this.dotaEnter);
			this.buttonDota.MouseLeave += new System.EventHandler(this.ButtonDota_MouseLeave);
			this.buttonDota.MouseHover += new System.EventHandler(this.dotaHover);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Location = new System.Drawing.Point(12, 38);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(97, 28);
			this.label2.TabIndex = 3;
			this.label2.Text = "Games";
			// 
			// buttonClose
			// 
			this.buttonClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.buttonClose.FlatAppearance.BorderSize = 0;
			this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClose.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonClose.ForeColor = System.Drawing.Color.Silver;
			this.buttonClose.Location = new System.Drawing.Point(917, 0);
			this.buttonClose.Margin = new System.Windows.Forms.Padding(0);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(35, 22);
			this.buttonClose.TabIndex = 2;
			this.buttonClose.Text = "X";
			this.buttonClose.UseVisualStyleBackColor = true;
			this.buttonClose.Click += new System.EventHandler(this.ButtonClose_Click);
			this.buttonClose.MouseEnter += new System.EventHandler(this.closeEnter);
			this.buttonClose.MouseLeave += new System.EventHandler(this.closeLeave);
			this.buttonClose.MouseHover += new System.EventHandler(this.closeHover);
			// 
			// topPanel
			// 
			this.topPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
			this.topPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.topPanel.Controls.Add(this.labelLogo);
			this.topPanel.Controls.Add(this.buttonMinimize);
			this.topPanel.Controls.Add(this.buttonClose);
			this.topPanel.Location = new System.Drawing.Point(0, 0);
			this.topPanel.Margin = new System.Windows.Forms.Padding(0);
			this.topPanel.Name = "topPanel";
			this.topPanel.Size = new System.Drawing.Size(952, 22);
			this.topPanel.TabIndex = 3;
			this.topPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mDown);
			this.topPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mMove);
			this.topPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mUp);
			// 
			// labelLogo
			// 
			this.labelLogo.AutoSize = true;
			this.labelLogo.Font = new System.Drawing.Font("Dungeon", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelLogo.ForeColor = System.Drawing.Color.White;
			this.labelLogo.Location = new System.Drawing.Point(3, 5);
			this.labelLogo.Name = "labelLogo";
			this.labelLogo.Size = new System.Drawing.Size(97, 13);
			this.labelLogo.TabIndex = 3;
			this.labelLogo.Text = "PingChecker";
			// 
			// buttonMinimize
			// 
			this.buttonMinimize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonMinimize.BackgroundImage")));
			this.buttonMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.buttonMinimize.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.buttonMinimize.FlatAppearance.BorderSize = 0;
			this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonMinimize.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonMinimize.ForeColor = System.Drawing.Color.White;
			this.buttonMinimize.Location = new System.Drawing.Point(883, 0);
			this.buttonMinimize.Margin = new System.Windows.Forms.Padding(0);
			this.buttonMinimize.Name = "buttonMinimize";
			this.buttonMinimize.Size = new System.Drawing.Size(34, 22);
			this.buttonMinimize.TabIndex = 2;
			this.buttonMinimize.UseVisualStyleBackColor = true;
			this.buttonMinimize.Click += new System.EventHandler(this.ButtonMin_Click);
			this.buttonMinimize.MouseEnter += new System.EventHandler(this.minEnter);
			this.buttonMinimize.MouseLeave += new System.EventHandler(this.minLeave);
			this.buttonMinimize.MouseHover += new System.EventHandler(this.minHover);
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(52)))), ((int)(((byte)(57)))));
			this.panel3.Controls.Add(this.labelPing);
			this.panel3.Controls.Add(this.labelPacketLoss);
			this.panel3.Controls.Add(this.labelAvgPing);
			this.panel3.Controls.Add(this.labelMinPing);
			this.panel3.Controls.Add(this.labelMaxPing);
			this.panel3.Controls.Add(this.label6);
			this.panel3.Controls.Add(this.label5);
			this.panel3.Controls.Add(this.label4);
			this.panel3.Controls.Add(this.label3);
			this.panel3.Controls.Add(this.label12);
			this.panel3.Controls.Add(this.label13);
			this.panel3.ForeColor = System.Drawing.Color.White;
			this.panel3.Location = new System.Drawing.Point(190, 296);
			this.panel3.Margin = new System.Windows.Forms.Padding(0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(762, 236);
			this.panel3.TabIndex = 8;
			// 
			// labelPing
			// 
			this.labelPing.AutoSize = true;
			this.labelPing.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelPing.ForeColor = System.Drawing.Color.White;
			this.labelPing.Location = new System.Drawing.Point(343, 66);
			this.labelPing.Name = "labelPing";
			this.labelPing.Size = new System.Drawing.Size(0, 25);
			this.labelPing.TabIndex = 21;
			this.labelPing.Tag = "";
			// 
			// labelPacketLoss
			// 
			this.labelPacketLoss.AutoSize = true;
			this.labelPacketLoss.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelPacketLoss.ForeColor = System.Drawing.Color.White;
			this.labelPacketLoss.Location = new System.Drawing.Point(636, 176);
			this.labelPacketLoss.Name = "labelPacketLoss";
			this.labelPacketLoss.Size = new System.Drawing.Size(0, 25);
			this.labelPacketLoss.TabIndex = 20;
			// 
			// labelAvgPing
			// 
			this.labelAvgPing.AutoSize = true;
			this.labelAvgPing.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelAvgPing.ForeColor = System.Drawing.Color.White;
			this.labelAvgPing.Location = new System.Drawing.Point(460, 176);
			this.labelAvgPing.Name = "labelAvgPing";
			this.labelAvgPing.Size = new System.Drawing.Size(0, 25);
			this.labelAvgPing.TabIndex = 19;
			// 
			// labelMinPing
			// 
			this.labelMinPing.AutoSize = true;
			this.labelMinPing.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelMinPing.ForeColor = System.Drawing.Color.White;
			this.labelMinPing.Location = new System.Drawing.Point(282, 176);
			this.labelMinPing.Name = "labelMinPing";
			this.labelMinPing.Size = new System.Drawing.Size(0, 25);
			this.labelMinPing.TabIndex = 18;
			// 
			// labelMaxPing
			// 
			this.labelMaxPing.AutoSize = true;
			this.labelMaxPing.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelMaxPing.ForeColor = System.Drawing.Color.White;
			this.labelMaxPing.Location = new System.Drawing.Point(95, 176);
			this.labelMaxPing.Name = "labelMaxPing";
			this.labelMaxPing.Size = new System.Drawing.Size(0, 25);
			this.labelMaxPing.TabIndex = 17;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(52)))), ((int)(((byte)(57)))));
			this.label6.Font = new System.Drawing.Font("Century Gothic", 18.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.ForeColor = System.Drawing.Color.White;
			this.label6.Location = new System.Drawing.Point(627, 126);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(62, 29);
			this.label6.TabIndex = 16;
			this.label6.Text = "Loss";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(52)))), ((int)(((byte)(57)))));
			this.label5.Font = new System.Drawing.Font("Century Gothic", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.White;
			this.label5.Location = new System.Drawing.Point(342, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(70, 32);
			this.label5.TabIndex = 15;
			this.label5.Text = "Ping";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Century Gothic", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.White;
			this.label4.Location = new System.Drawing.Point(778, 35);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(79, 38);
			this.label4.TabIndex = 14;
			this.label4.Text = "Loss";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(52)))), ((int)(((byte)(57)))));
			this.label3.Font = new System.Drawing.Font("Century Gothic", 18.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.White;
			this.label3.Location = new System.Drawing.Point(435, 126);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(121, 29);
			this.label3.TabIndex = 13;
			this.label3.Text = "Avg Ping";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(52)))), ((int)(((byte)(57)))));
			this.label12.Font = new System.Drawing.Font("Century Gothic", 18.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.ForeColor = System.Drawing.Color.White;
			this.label12.Location = new System.Drawing.Point(262, 126);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(115, 29);
			this.label12.TabIndex = 12;
			this.label12.Text = "Min Ping";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(52)))), ((int)(((byte)(57)))));
			this.label13.Font = new System.Drawing.Font("Century Gothic", 18.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.ForeColor = System.Drawing.Color.White;
			this.label13.Location = new System.Drawing.Point(80, 126);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(125, 29);
			this.label13.TabIndex = 11;
			this.label13.Text = "Max Ping";
			// 
			// panelDota
			// 
			this.panelDota.Controls.Add(this.panelPubg);
			this.panelDota.Controls.Add(this.buttonCheckPingDota);
			this.panelDota.Controls.Add(this.label15);
			this.panelDota.Controls.Add(this.comboBoxDota2);
			this.panelDota.Controls.Add(this.pictureDota);
			this.panelDota.Location = new System.Drawing.Point(189, 22);
			this.panelDota.Margin = new System.Windows.Forms.Padding(0);
			this.panelDota.Name = "panelDota";
			this.panelDota.Size = new System.Drawing.Size(761, 274);
			this.panelDota.TabIndex = 9;
			// 
			// panelPubg
			// 
			this.panelPubg.Controls.Add(this.buttonCheckPingPubg);
			this.panelPubg.Controls.Add(this.label14);
			this.panelPubg.Controls.Add(this.comboBoxPubg);
			this.panelPubg.Controls.Add(this.pictureBox1);
			this.panelPubg.Location = new System.Drawing.Point(0, 0);
			this.panelPubg.Margin = new System.Windows.Forms.Padding(0);
			this.panelPubg.Name = "panelPubg";
			this.panelPubg.Size = new System.Drawing.Size(761, 274);
			this.panelPubg.TabIndex = 14;
			this.panelPubg.Visible = false;
			// 
			// buttonCheckPingPubg
			// 
			this.buttonCheckPingPubg.BackColor = System.Drawing.Color.Transparent;
			this.buttonCheckPingPubg.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.buttonCheckPingPubg.FlatAppearance.BorderSize = 2;
			this.buttonCheckPingPubg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonCheckPingPubg.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonCheckPingPubg.ForeColor = System.Drawing.Color.White;
			this.buttonCheckPingPubg.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.buttonCheckPingPubg.Location = new System.Drawing.Point(67, 188);
			this.buttonCheckPingPubg.Margin = new System.Windows.Forms.Padding(0);
			this.buttonCheckPingPubg.Name = "buttonCheckPingPubg";
			this.buttonCheckPingPubg.Size = new System.Drawing.Size(166, 44);
			this.buttonCheckPingPubg.TabIndex = 13;
			this.buttonCheckPingPubg.Text = "Check Ping";
			this.buttonCheckPingPubg.UseVisualStyleBackColor = false;
			this.buttonCheckPingPubg.Click += new System.EventHandler(this.buttonCheckPingPubg_Click);
			this.buttonCheckPingPubg.MouseEnter += new System.EventHandler(this.ButtonCheckPingPubg_MouseEnter);
			this.buttonCheckPingPubg.MouseLeave += new System.EventHandler(this.ButtonCheckPingPubg_MouseLeave);
			this.buttonCheckPingPubg.MouseHover += new System.EventHandler(this.ButtonCheckPingPubg_MouseHover);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Century Gothic", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.ForeColor = System.Drawing.Color.White;
			this.label14.Location = new System.Drawing.Point(20, 62);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(88, 29);
			this.label14.TabIndex = 12;
			this.label14.Text = "Server";
			// 
			// comboBoxPubg
			// 
			this.comboBoxPubg.Font = new System.Drawing.Font("Century Gothic", 11.25F);
			this.comboBoxPubg.FormattingEnabled = true;
			this.comboBoxPubg.Items.AddRange(new object[] {
            "Asia",
            "Europe",
            "Soth America",
            "North America",
            "Australia",
            "Korea",
            "Japan"});
			this.comboBoxPubg.Location = new System.Drawing.Point(114, 65);
			this.comboBoxPubg.Name = "comboBoxPubg";
			this.comboBoxPubg.Size = new System.Drawing.Size(138, 28);
			this.comboBoxPubg.TabIndex = 11;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBox1.Location = new System.Drawing.Point(282, 13);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(460, 245);
			this.pictureBox1.TabIndex = 6;
			this.pictureBox1.TabStop = false;
			// 
			// buttonCheckPingDota
			// 
			this.buttonCheckPingDota.BackColor = System.Drawing.Color.Transparent;
			this.buttonCheckPingDota.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.buttonCheckPingDota.FlatAppearance.BorderSize = 2;
			this.buttonCheckPingDota.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonCheckPingDota.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonCheckPingDota.ForeColor = System.Drawing.Color.White;
			this.buttonCheckPingDota.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.buttonCheckPingDota.Location = new System.Drawing.Point(67, 188);
			this.buttonCheckPingDota.Margin = new System.Windows.Forms.Padding(0);
			this.buttonCheckPingDota.Name = "buttonCheckPingDota";
			this.buttonCheckPingDota.Size = new System.Drawing.Size(166, 44);
			this.buttonCheckPingDota.TabIndex = 13;
			this.buttonCheckPingDota.Text = "Check Ping";
			this.buttonCheckPingDota.UseVisualStyleBackColor = false;
			this.buttonCheckPingDota.Click += new System.EventHandler(this.ButtonCheckPingDota_Click);
			this.buttonCheckPingDota.MouseEnter += new System.EventHandler(this.ButtonCheckPingDota_MouseEnter);
			this.buttonCheckPingDota.MouseLeave += new System.EventHandler(this.ButtonCheckPingDota_MouseLeave);
			this.buttonCheckPingDota.MouseHover += new System.EventHandler(this.ButtonCheckPingDota_MouseHover);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Century Gothic", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.ForeColor = System.Drawing.Color.White;
			this.label15.Location = new System.Drawing.Point(20, 62);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(88, 29);
			this.label15.TabIndex = 12;
			this.label15.Text = "Server";
			// 
			// comboBoxDota2
			// 
			this.comboBoxDota2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboBoxDota2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
			this.comboBoxDota2.FormattingEnabled = true;
			this.comboBoxDota2.Items.AddRange(new object[] {
            "Dubai",
            "SE Asia",
            "Russia",
            "Europe East",
            "Europe West",
            "Australia",
            "South Africa",
            "South America",
            "US East",
            "US West"});
			this.comboBoxDota2.Location = new System.Drawing.Point(114, 65);
			this.comboBoxDota2.Name = "comboBoxDota2";
			this.comboBoxDota2.Size = new System.Drawing.Size(138, 28);
			this.comboBoxDota2.TabIndex = 11;
			// 
			// pictureDota
			// 
			this.pictureDota.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureDota.BackgroundImage")));
			this.pictureDota.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureDota.Location = new System.Drawing.Point(282, 13);
			this.pictureDota.Name = "pictureDota";
			this.pictureDota.Size = new System.Drawing.Size(460, 245);
			this.pictureDota.TabIndex = 6;
			this.pictureDota.TabStop = false;
			// 
			// timer1
			// 
			this.timer1.Interval = 500;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// mainWindow
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
			this.ClientSize = new System.Drawing.Size(951, 532);
			this.Controls.Add(this.panelDota);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.topPanel);
			this.Controls.Add(this.dashBoard);
			this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "mainWindow";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.dashBoard.ResumeLayout(false);
			this.dashBoard.PerformLayout();
			this.topPanel.ResumeLayout(false);
			this.topPanel.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panelDota.ResumeLayout(false);
			this.panelDota.PerformLayout();
			this.panelPubg.ResumeLayout(false);
			this.panelPubg.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureDota)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel dashBoard;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Panel topPanel;
		private System.Windows.Forms.Label labelLogo;
		private System.Windows.Forms.Button buttonDota;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonApex;
		private System.Windows.Forms.Button buttonPubg;
		private System.Windows.Forms.Button buttonMinimize;
		private System.Windows.Forms.Label label2;
		private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.ComponentModel.BackgroundWorker backgroundWorker2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label labelPing;
		private System.Windows.Forms.Label labelPacketLoss;
		private System.Windows.Forms.Label labelAvgPing;
		private System.Windows.Forms.Label labelMinPing;
		private System.Windows.Forms.Label labelMaxPing;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panelDota;
		private System.Windows.Forms.Button buttonCheckPingDota;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.ComboBox comboBoxDota2;
		private System.Windows.Forms.PictureBox pictureDota;
		private System.Windows.Forms.Panel panelPubg;
		private System.Windows.Forms.Button buttonCheckPingPubg;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox comboBoxPubg;
		private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
	}
}

