﻿namespace pingChecker2._0
{
	partial class FormNoInternet
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelNoInternet = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonOkNo = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// labelNoInternet
			// 
			this.labelNoInternet.AutoSize = true;
			this.labelNoInternet.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelNoInternet.ForeColor = System.Drawing.Color.White;
			this.labelNoInternet.Location = new System.Drawing.Point(27, 24);
			this.labelNoInternet.Name = "labelNoInternet";
			this.labelNoInternet.Size = new System.Drawing.Size(280, 28);
			this.labelNoInternet.TabIndex = 13;
			this.labelNoInternet.Text = "No Internet Connection";
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.White;
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(5, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(324, 5);
			this.panel4.TabIndex = 18;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.White;
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(5, 124);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(324, 5);
			this.panel2.TabIndex = 19;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.White;
			this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(5, 129);
			this.panel3.TabIndex = 20;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(329, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(5, 129);
			this.panel1.TabIndex = 21;
			// 
			// buttonOkNo
			// 
			this.buttonOkNo.BackColor = System.Drawing.Color.Transparent;
			this.buttonOkNo.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.buttonOkNo.FlatAppearance.BorderSize = 2;
			this.buttonOkNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOkNo.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonOkNo.ForeColor = System.Drawing.Color.White;
			this.buttonOkNo.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.buttonOkNo.Location = new System.Drawing.Point(131, 70);
			this.buttonOkNo.Margin = new System.Windows.Forms.Padding(0);
			this.buttonOkNo.Name = "buttonOkNo";
			this.buttonOkNo.Size = new System.Drawing.Size(68, 32);
			this.buttonOkNo.TabIndex = 22;
			this.buttonOkNo.Text = "OK";
			this.buttonOkNo.UseVisualStyleBackColor = false;
			this.buttonOkNo.Click += new System.EventHandler(this.ButtonOkNo_Click);
			this.buttonOkNo.MouseEnter += new System.EventHandler(this.ButtonOk_MouseEnter);
			this.buttonOkNo.MouseLeave += new System.EventHandler(this.ButtonOk_MouseLeave);
			this.buttonOkNo.MouseHover += new System.EventHandler(this.ButtonOk_MouseHover);
			// 
			// FormNoInternet
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(45)))), ((int)(((byte)(50)))));
			this.ClientSize = new System.Drawing.Size(334, 129);
			this.Controls.Add(this.buttonOkNo);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.labelNoInternet);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "FormNoInternet";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "FormNoInternet";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelNoInternet;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonOkNo;
	}
}